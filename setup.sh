#!/bin/bash
# script=$(curl -L https://gitlab.com/dvblk/share/-/raw/main/setup.sh) && bash -c "$script"

gitlabUsername=dvblk

echo "Please provide your password:"
read -s password

if ! command -v nix-env &> /dev/null; then
    echo "Installing nix..."
    echo "$password" | sudo -S mkdir -m 0755 /nix 
    echo "$password" | sudo -S chown $USER /nix
    sh <(curl -L https://nixos.org/nix/install) --no-daemon
    echo "Setting nixpkgs to 24.11..."
    nix-channel --add https://nixos.org/channels/nixpkgs-24.05-darwin nixpkgs
    nix-channel --update
fi

if [ ! -d ~/.config ]; then
    echo "Creating ~/.config/ directory..."
    mkdir -p ~/.config
fi

if [ ! -d ~/.config/nix ]; then
    echo "Creating ~/.config/nix directory..."
    mkdir -p ~/.config/nix
fi

config_file='$HOME/.config/nix/nix.conf'
if [ ! -e "$config_file" ]; then
    echo "Adding line to $config_file"
    echo "experimental-features = nix-command flakes" > "$config_file"
fi

. $HOME/.nix-profile/etc/profile.d/nix.sh

if ! command -v home-manager &> /dev/null; then
    echo "Installing home-manager..."
    nix-channel --add https://github.com/nix-community/home-manager/archive/release-24.05.tar.gz home-manager
    nix-channel --update
    nix-shell '<home-manager>' -A install
    nix run home-manager/master -- init --switch
fi

if ! command -v git &> /dev/null; then
    echo "Installing git..."
    nix-env -iA nixpkgs.git
fi

if ! command -v ssh &> /dev/null; then
    echo "Installing ssh..."
    nix-env -iA nixpkgs.openssh
fi

if [ ! -d ~/git ]; then
    echo "Creating ~/git directory..."
    mkdir -p ~/git
fi

function secrets {
  /usr/bin/git --git-dir=$HOME/.secrets/ --work-tree=$HOME $@
}

function config {
  /usr/bin/git --git-dir=$HOME/.dots/ --work-tree=$HOME $@
}

if [ ! -d ~/.secrets ]; then
  echo "Cloning secrets repo..."
  git clone --bare https://$gitlabUsername:$password@gitlab.com/$gitlabUsername/secrets.git $HOME/.secrets
  secrets remote set-url origin git@gitlab.com:dvblk/secrets.git
  secrets checkout
  secrets config status.showUntrackedFiles no
  chmod 600 $HOME/.ssh/id_rsa
fi

if [ ! -d ~/.dots ]; then
  echo "Cloning config repo..."
  git clone --bare git@gitlab.com:dvblk/config.git $HOME/.dots
  mkdir -p .config-backup
  config checkout 2>&1 | egrep "\s+\." | awk {'print $1'} | xargs -I{} sh -c 'mkdir -p "$(dirname .config-backup/{})" && mv {} .config-backup/{}'
  config checkout
  config config status.showUntrackedFiles no
fi


if [ ! -d ~/git/share ]; then
  echo "Cloning share repo..."
  git clone git@gitlab.com:dvblk/share.git ~/git/share
fi

home-manager switch

nvim +qall

tmux
